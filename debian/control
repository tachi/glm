Source: glm
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Andrea Pappacoda <andrea@pappacoda.it>
Section: math
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: cmake (>= 3.20),
                     cmark <!nodoc>,
                     doxygen <!nodoc>
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/science-team/glm
Vcs-Git: https://salsa.debian.org/science-team/glm.git
Homepage: https://glm.g-truc.net/
Rules-Requires-Root: no

Package: libglm-dev
Architecture: all
Multi-Arch: foreign
Section: libdevel
Depends: ${misc:Depends}
Description: C++ library for OpenGL GLSL type-based mathematics
 OpenGL Mathematics (GLM) is a header only C++ mathematics library
 for graphics software based on the OpenGL Shading Language (GLSL)
 specification.
 .
 GLM provides classes and functions designed and implemented with the
 same naming conventions and functionality than GLSL so that anyone who
 knows GLSL, can use GLM as well in C++.
 .
 This project isn't limited to GLSL features. An extension system, based
 on the GLSL extension conventions, provides extended capabilities: matrix
 transformations, quaternions, data packing, random numbers, noise, etc...
 .
 This library works perfectly with OpenGL but it also ensures
 interoperability with other third party libraries and SDK. It is a
 good candidate for software rendering (raytracing / rasterisation),
 image processing, physics simulations and any development context that
 requires a simple and convenient mathematics library.

Package: libglm-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Recommends: libjs-jquery
Description: documentation for the OpenGL Mathematics (GLM) library
 OpenGL Mathematics (GLM) is a header only C++ mathematics library
 for graphics software based on the OpenGL Shading Language (GLSL)
 specification.
 .
 This package contains the OpenGL Mathematics manual and API documentation
 in HTML format.
