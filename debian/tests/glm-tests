#!/bin/sh
# Copyright 2016 Ghislain Antony Vaillant
#
# This file is part of the packaging testsuite for glm.

set -eu

# Copy the upstream testsuite.
cp -a ./test/* "$AUTOPKGTEST_TMP"

cd "$AUTOPKGTEST_TMP"

# Create a standalone CMake project.
rm CMakeLists.txt

cat << EOF > CMakeLists.txt
cmake_minimum_required(VERSION 3.0)
project(glm-tests)

enable_testing()

find_package(glm REQUIRED)
link_libraries(glm::glm)
include_directories(external)

function(glmCreateTestGTC NAME)
  set(SAMPLE_NAME test-\${NAME})
  add_executable(\${SAMPLE_NAME} \${NAME}.cpp)
  add_test(NAME \${SAMPLE_NAME}
           COMMAND \$<TARGET_FILE:\${SAMPLE_NAME}>)
endfunction()

add_subdirectory(bug)
add_subdirectory(core)
add_subdirectory(gtc)
add_subdirectory(gtx)

EOF

# Configure, build and execute.
cmake -B build
cd build
# stderr is redirected to stdout because the tests have some harmless
#`#pragma message` directives that are sent to stderr
cmake --build . 2>&1
ctest
